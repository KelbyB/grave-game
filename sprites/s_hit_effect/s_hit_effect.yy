{
    "id": "91d92920-8b8a-4b8b-89a1-12b710091774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hit_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1ef1214-368a-4d92-92fd-2fbe98dd665e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91d92920-8b8a-4b8b-89a1-12b710091774",
            "compositeImage": {
                "id": "7909a2be-19d4-4733-9131-8ea7579a2c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ef1214-368a-4d92-92fd-2fbe98dd665e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c786ba9-f1bd-4ce8-a026-18a48898ee71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ef1214-368a-4d92-92fd-2fbe98dd665e",
                    "LayerId": "d6ac464d-368a-4244-83a4-da042e88dd45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "d6ac464d-368a-4244-83a4-da042e88dd45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91d92920-8b8a-4b8b-89a1-12b710091774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 2
}