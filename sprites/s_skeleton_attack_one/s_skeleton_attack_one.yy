{
    "id": "03e8069d-bea2-4655-819a-6672b6d9ee26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 15,
    "bbox_right": 63,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebab1416-688a-482c-a951-50f9f018aa2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "compositeImage": {
                "id": "ad1bfeca-b806-40b6-8fda-86b27b2a1408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebab1416-688a-482c-a951-50f9f018aa2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57527ae0-2bce-485f-95c2-4a68778ef5f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebab1416-688a-482c-a951-50f9f018aa2d",
                    "LayerId": "d26c925a-e81a-445d-9cab-365875b5ad3c"
                }
            ]
        },
        {
            "id": "a4dcffda-b26b-4e73-aa72-a64cbcc4a17a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "compositeImage": {
                "id": "08018de8-afba-495f-acf4-831e76b90868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4dcffda-b26b-4e73-aa72-a64cbcc4a17a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb53db8-fa0c-40aa-b629-9fc2de522b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4dcffda-b26b-4e73-aa72-a64cbcc4a17a",
                    "LayerId": "d26c925a-e81a-445d-9cab-365875b5ad3c"
                }
            ]
        },
        {
            "id": "167710f9-d609-4b34-a7bb-c4034e28ed5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "compositeImage": {
                "id": "92258d81-ee24-4f7f-8dcf-186865a80347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167710f9-d609-4b34-a7bb-c4034e28ed5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c27cd60-ffdc-4ff0-b1f0-60aa74051049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167710f9-d609-4b34-a7bb-c4034e28ed5c",
                    "LayerId": "d26c925a-e81a-445d-9cab-365875b5ad3c"
                }
            ]
        },
        {
            "id": "fb4fbcaf-d4dc-4c3c-bf59-501c963a40a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "compositeImage": {
                "id": "4acc8bca-501d-41f9-a169-cdef75e97b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4fbcaf-d4dc-4c3c-bf59-501c963a40a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e21f57-a233-4d03-92c7-2d1e0e41c3db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4fbcaf-d4dc-4c3c-bf59-501c963a40a9",
                    "LayerId": "d26c925a-e81a-445d-9cab-365875b5ad3c"
                }
            ]
        },
        {
            "id": "8772c9cd-addd-461c-910d-85094296b3df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "compositeImage": {
                "id": "171f9a65-733e-4ca4-8df8-fbf9fe2ca5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8772c9cd-addd-461c-910d-85094296b3df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866699e3-55fc-46e4-baa6-ba3e05255aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8772c9cd-addd-461c-910d-85094296b3df",
                    "LayerId": "d26c925a-e81a-445d-9cab-365875b5ad3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d26c925a-e81a-445d-9cab-365875b5ad3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03e8069d-bea2-4655-819a-6672b6d9ee26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 24,
    "yorig": 48
}