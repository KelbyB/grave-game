{
    "id": "65655a1a-45ca-4aec-8354-cd545980522f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_graves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03951af2-749d-4b4e-b4c8-d2d96306b1cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65655a1a-45ca-4aec-8354-cd545980522f",
            "compositeImage": {
                "id": "5809655e-224c-43c8-bf78-bde909800acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03951af2-749d-4b4e-b4c8-d2d96306b1cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e9f7f67-afb5-42d6-af74-eac60a335423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03951af2-749d-4b4e-b4c8-d2d96306b1cd",
                    "LayerId": "13a50caa-4c5c-41cc-98cf-e02741912c43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "13a50caa-4c5c-41cc-98cf-e02741912c43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65655a1a-45ca-4aec-8354-cd545980522f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}