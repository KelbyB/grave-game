{
    "id": "1be2b772-21b7-4364-af0c-8402a1495376",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_crow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b2547c7-bede-4c6a-a99e-9823d50f4862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "compositeImage": {
                "id": "54c7e4c3-e73a-4c34-a5ba-1fe421f6777f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2547c7-bede-4c6a-a99e-9823d50f4862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958dc684-44e7-4017-87fb-0c8e2d890a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2547c7-bede-4c6a-a99e-9823d50f4862",
                    "LayerId": "b757b30a-e0cc-4299-ad05-d558b1909603"
                }
            ]
        },
        {
            "id": "e2a7543a-a71f-4dfe-9fca-f55be9492e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "compositeImage": {
                "id": "8e4f2fbf-2f44-47e1-802c-7693bee4b7f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2a7543a-a71f-4dfe-9fca-f55be9492e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d518fcd2-5549-43a0-8535-be0f1d323ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2a7543a-a71f-4dfe-9fca-f55be9492e3e",
                    "LayerId": "b757b30a-e0cc-4299-ad05-d558b1909603"
                }
            ]
        },
        {
            "id": "f72b4322-89eb-4985-b058-5419a0a5a4be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "compositeImage": {
                "id": "216e6c80-106b-4299-9d64-67cb184396c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f72b4322-89eb-4985-b058-5419a0a5a4be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d002c090-cc8d-457a-8d7d-c73124e97dc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f72b4322-89eb-4985-b058-5419a0a5a4be",
                    "LayerId": "b757b30a-e0cc-4299-ad05-d558b1909603"
                }
            ]
        },
        {
            "id": "e7ecbe37-befc-4d63-9b92-9f4cfbba45be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "compositeImage": {
                "id": "3f1a4fc3-83ed-4cb3-ac55-eecbfb702773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ecbe37-befc-4d63-9b92-9f4cfbba45be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d534f7b-1589-4714-a9b4-0d154e944aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ecbe37-befc-4d63-9b92-9f4cfbba45be",
                    "LayerId": "b757b30a-e0cc-4299-ad05-d558b1909603"
                }
            ]
        },
        {
            "id": "5b40449f-b3f8-46b0-b2c0-d53e909a6c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "compositeImage": {
                "id": "704337bc-daf5-4894-b5a6-346a093b3e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b40449f-b3f8-46b0-b2c0-d53e909a6c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487f0c25-f978-4196-bf06-23de10df037a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b40449f-b3f8-46b0-b2c0-d53e909a6c43",
                    "LayerId": "b757b30a-e0cc-4299-ad05-d558b1909603"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b757b30a-e0cc-4299-ad05-d558b1909603",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 47
}