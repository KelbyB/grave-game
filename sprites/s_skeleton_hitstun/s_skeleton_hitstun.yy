{
    "id": "699128f0-e070-4cca-8382-04ec8010d0a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 36,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a5c3140-9130-460f-a76b-1314c59d9e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "699128f0-e070-4cca-8382-04ec8010d0a2",
            "compositeImage": {
                "id": "31deb685-c9a2-4107-aacc-ad4563d4ccd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a5c3140-9130-460f-a76b-1314c59d9e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae026040-e7cf-4335-9628-1e3d0b50a372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a5c3140-9130-460f-a76b-1314c59d9e48",
                    "LayerId": "94bea394-1d3d-4b5c-a074-2450562ad69d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "94bea394-1d3d-4b5c-a074-2450562ad69d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "699128f0-e070-4cca-8382-04ec8010d0a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 48
}