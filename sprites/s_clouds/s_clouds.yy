{
    "id": "7cbae7c2-43fd-41e3-aef5-9107ad27b5a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_clouds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 20,
    "bbox_right": 628,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76490e96-567f-4aa1-ae7e-c6987f57e93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cbae7c2-43fd-41e3-aef5-9107ad27b5a0",
            "compositeImage": {
                "id": "e7675d20-fbf0-40ef-89b2-c13eae196754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76490e96-567f-4aa1-ae7e-c6987f57e93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a989a98-3ae6-44c8-8e7c-9a85644e8e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76490e96-567f-4aa1-ae7e-c6987f57e93c",
                    "LayerId": "ffdb5878-d54e-48b1-876c-122f4dab8236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "ffdb5878-d54e-48b1-876c-122f4dab8236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cbae7c2-43fd-41e3-aef5-9107ad27b5a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}