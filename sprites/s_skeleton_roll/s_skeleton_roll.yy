{
    "id": "30936784-4b93-4dd4-992d-400e0b8d28d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_roll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25600058-30a2-453a-b191-3bf8cb6714dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "c4fcb9ed-209e-4641-88a2-08bd22ce72dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25600058-30a2-453a-b191-3bf8cb6714dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff12f8e3-6e01-4853-917b-40eb0cec88ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25600058-30a2-453a-b191-3bf8cb6714dd",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "e1e8b9cc-c94f-4d2b-8d1e-fa08a3827df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "2ed901bf-ad3e-4b35-8e92-2297cb885b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e8b9cc-c94f-4d2b-8d1e-fa08a3827df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c74abb-2284-46ed-b203-2e3062799346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e8b9cc-c94f-4d2b-8d1e-fa08a3827df1",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "30c5b341-c2d5-4597-952a-7fcb16c52763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "5f6def66-492f-44fd-8988-23aca889b897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30c5b341-c2d5-4597-952a-7fcb16c52763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e74c2b4-3b32-433e-aa5d-ff7edb960fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30c5b341-c2d5-4597-952a-7fcb16c52763",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "55609e88-7a92-4d3f-b998-739aedad6904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "f62da33c-6241-4ea7-aafa-313ad2dceb16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55609e88-7a92-4d3f-b998-739aedad6904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd704d7-0002-4cea-9072-d27704edda1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55609e88-7a92-4d3f-b998-739aedad6904",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "0558bb2b-4378-47e8-86c9-be894421e4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "35ee7464-cf94-4121-a697-b6c0d18995ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0558bb2b-4378-47e8-86c9-be894421e4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b85f823-d208-49ed-9136-3e448b124670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0558bb2b-4378-47e8-86c9-be894421e4e6",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "e22d2ec8-21eb-401b-9daa-97e3932f6ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "3b39aed0-fdae-4b2b-9164-3be1c740d31e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22d2ec8-21eb-401b-9daa-97e3932f6ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "439e582d-43a1-40ac-b825-c04186392ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22d2ec8-21eb-401b-9daa-97e3932f6ec3",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        },
        {
            "id": "5a39853f-a3f6-40d8-91a8-7e0efd9ca4f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "compositeImage": {
                "id": "4297d2c9-b8bc-4619-ba99-05e810b7554e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a39853f-a3f6-40d8-91a8-7e0efd9ca4f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc723a87-c2f9-4e42-9c4f-d2b26832196e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a39853f-a3f6-40d8-91a8-7e0efd9ca4f2",
                    "LayerId": "f58857c6-ac37-47b9-bc4a-59b2260b719d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f58857c6-ac37-47b9-bc4a-59b2260b719d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30936784-4b93-4dd4-992d-400e0b8d28d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 48
}