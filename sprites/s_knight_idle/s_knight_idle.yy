{
    "id": "2a5c8229-488b-46cb-9774-272a5cd043d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 33,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c51409-6513-44bc-8b90-080897bb629d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5c8229-488b-46cb-9774-272a5cd043d1",
            "compositeImage": {
                "id": "396f7911-e90c-4031-9393-d8a4a56dc754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c51409-6513-44bc-8b90-080897bb629d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5db7ddb3-9426-4d04-bab1-416894725165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c51409-6513-44bc-8b90-080897bb629d",
                    "LayerId": "6284449c-b292-43ea-ad2d-55b8beb69be5"
                }
            ]
        },
        {
            "id": "52f79c01-0ce7-4edc-9cd8-d63fbae46890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5c8229-488b-46cb-9774-272a5cd043d1",
            "compositeImage": {
                "id": "76bff8bb-f912-4d79-a022-c4597d6a1d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f79c01-0ce7-4edc-9cd8-d63fbae46890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461b721a-39c4-41c4-b525-6650401e13b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f79c01-0ce7-4edc-9cd8-d63fbae46890",
                    "LayerId": "6284449c-b292-43ea-ad2d-55b8beb69be5"
                }
            ]
        },
        {
            "id": "e46dfb80-4620-43b9-8dcf-42254f1988fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5c8229-488b-46cb-9774-272a5cd043d1",
            "compositeImage": {
                "id": "c244766f-e795-4e6c-b0ae-85c375673894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e46dfb80-4620-43b9-8dcf-42254f1988fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9ccba95-cf38-4aa5-87d0-87071ea99a03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e46dfb80-4620-43b9-8dcf-42254f1988fb",
                    "LayerId": "6284449c-b292-43ea-ad2d-55b8beb69be5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6284449c-b292-43ea-ad2d-55b8beb69be5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a5c8229-488b-46cb-9774-272a5cd043d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 48
}