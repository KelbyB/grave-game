{
    "id": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "522711f3-3b41-4f14-b49a-f7d29a73704a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
            "compositeImage": {
                "id": "bf49a4dd-f7e5-4b17-9a6d-31fa668b49b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522711f3-3b41-4f14-b49a-f7d29a73704a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623130bd-c81a-48bf-adb8-c25786ee2923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522711f3-3b41-4f14-b49a-f7d29a73704a",
                    "LayerId": "9dc71c50-4855-465b-9690-86d3343fc8df"
                }
            ]
        },
        {
            "id": "56c277a1-c92a-4ede-943f-f1c1b94edaff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
            "compositeImage": {
                "id": "888b1b3b-b855-432b-8ce4-013072be8220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c277a1-c92a-4ede-943f-f1c1b94edaff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef870e64-7fe8-44c6-93a5-c0dddbbcf338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c277a1-c92a-4ede-943f-f1c1b94edaff",
                    "LayerId": "9dc71c50-4855-465b-9690-86d3343fc8df"
                }
            ]
        },
        {
            "id": "7e9f24c2-bfae-410a-bc13-b0d9c4d5298c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
            "compositeImage": {
                "id": "e8723cfe-9127-417e-9f36-c4e5b09672a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9f24c2-bfae-410a-bc13-b0d9c4d5298c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "256551a9-c7b4-4c91-bbbb-f51460b8e417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9f24c2-bfae-410a-bc13-b0d9c4d5298c",
                    "LayerId": "9dc71c50-4855-465b-9690-86d3343fc8df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "9dc71c50-4855-465b-9690-86d3343fc8df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 48
}