{
    "id": "36d3347c-1944-4292-91a8-558e5a220e83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_one_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 11,
    "bbox_right": 63,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd2035a-7ab1-4420-bf42-4ff925ec8f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d3347c-1944-4292-91a8-558e5a220e83",
            "compositeImage": {
                "id": "988f6ffa-7888-4a2b-a1e3-cba8af15be82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd2035a-7ab1-4420-bf42-4ff925ec8f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0defdb1-a9e0-4df5-b85f-550ff1cc8ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd2035a-7ab1-4420-bf42-4ff925ec8f05",
                    "LayerId": "51c55896-d8e6-439c-9f58-cb5d100966d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "51c55896-d8e6-439c-9f58-cb5d100966d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36d3347c-1944-4292-91a8-558e5a220e83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 24,
    "yorig": 48
}