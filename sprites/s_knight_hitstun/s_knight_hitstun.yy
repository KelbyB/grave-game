{
    "id": "0668afea-7a59-453c-8726-aa9293a8a003",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 2,
    "bbox_right": 32,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edbc7f3a-0fc8-4428-868c-07d742976a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0668afea-7a59-453c-8726-aa9293a8a003",
            "compositeImage": {
                "id": "ca74ab6f-fdfa-489f-b66d-202db87227ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbc7f3a-0fc8-4428-868c-07d742976a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e75e7f6-1e31-4697-a840-02e21382fc7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbc7f3a-0fc8-4428-868c-07d742976a3d",
                    "LayerId": "c2b70997-8436-49c6-a268-08b1ed48e67e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c2b70997-8436-49c6-a268-08b1ed48e67e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0668afea-7a59-453c-8726-aa9293a8a003",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 48
}