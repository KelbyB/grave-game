{
    "id": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 58,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecdb688f-c4ce-492d-9726-11b1ba277e1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "3dc665cf-a9bb-48a0-a943-e620a6523b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecdb688f-c4ce-492d-9726-11b1ba277e1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4293c53f-3d4f-40c5-875c-0af7d336b2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecdb688f-c4ce-492d-9726-11b1ba277e1d",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "c6465f22-7798-4bc6-9138-d95999563e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "23c90183-bfe3-441b-aeb2-d6600b4804ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6465f22-7798-4bc6-9138-d95999563e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45db02a5-d6d9-4b25-9bec-5d7a3ce76db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6465f22-7798-4bc6-9138-d95999563e69",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "602f6597-d37e-40da-a564-6ca28b46873c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "39185d1f-3fb6-43bd-9d1e-7bf564a2ac5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602f6597-d37e-40da-a564-6ca28b46873c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70720119-9f4e-4449-a9b8-dc930947eb46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602f6597-d37e-40da-a564-6ca28b46873c",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "592fbf7c-3aae-4730-b837-12e88ffac4e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "bbc2642e-1a88-4ae1-9c6e-c8c5688788b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592fbf7c-3aae-4730-b837-12e88ffac4e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa96f5c0-42f8-4490-9f23-eda7b70e5ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592fbf7c-3aae-4730-b837-12e88ffac4e9",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "01dcfceb-11c6-4d4e-b019-93b7decfbdb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "15c786e7-0068-456f-9059-c93fbd197f39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01dcfceb-11c6-4d4e-b019-93b7decfbdb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c01b44-b9df-4487-9e9a-86a786c67119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01dcfceb-11c6-4d4e-b019-93b7decfbdb3",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "96ea4dc1-066b-410c-b5c7-99585a56cca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "ad9c61e0-3011-42da-b176-0c83ea8a0c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ea4dc1-066b-410c-b5c7-99585a56cca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36bea9ef-de8b-4ecf-a3e7-995fd4123561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ea4dc1-066b-410c-b5c7-99585a56cca1",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "cf6e6e1e-ddc4-47e5-9e04-11d013e051c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "dab6ed7e-b1a5-481a-984b-2b530118fb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6e6e1e-ddc4-47e5-9e04-11d013e051c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a6e0d4-5732-447d-9bdc-c7b7e0a2604e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6e6e1e-ddc4-47e5-9e04-11d013e051c2",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "76fe981b-becd-4327-a907-4f3b945bf3e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "e6f0ea9d-40b8-4108-a0d3-ae667e4e53b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fe981b-becd-4327-a907-4f3b945bf3e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f6ae9a-6728-43a8-ae7a-627b2e97625c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fe981b-becd-4327-a907-4f3b945bf3e9",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "e2f38ae9-60e3-455e-8823-67f03ea83b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "e6c5e1aa-3ed2-488a-8b4f-6ea952233a5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f38ae9-60e3-455e-8823-67f03ea83b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad65ab29-f032-4084-9903-a460267b1f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f38ae9-60e3-455e-8823-67f03ea83b6b",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "bc2443dd-32ec-4089-ad2f-b0394097d171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "2b480309-6e48-4f0d-8a22-cc6f91dda104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2443dd-32ec-4089-ad2f-b0394097d171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa0c10e-68d8-4bb2-90a2-e0924a8c7068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2443dd-32ec-4089-ad2f-b0394097d171",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "7c2c0686-3497-4393-a72a-621db16cf7bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "247b2206-17df-4d7a-966d-b6d063298d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2c0686-3497-4393-a72a-621db16cf7bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f6bc63-99db-4742-b534-0de1bc30c8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2c0686-3497-4393-a72a-621db16cf7bd",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        },
        {
            "id": "a6ee20ee-a627-43e1-a738-9187e4c176fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "compositeImage": {
                "id": "360be654-0a5c-4b57-9e22-9275e5029900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ee20ee-a627-43e1-a738-9187e4c176fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73564abc-2011-435e-a10a-d4d3571b24bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ee20ee-a627-43e1-a738-9187e4c176fe",
                    "LayerId": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7d2d6c41-dfc6-42c4-9a8b-125019b187d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf238bad-4cd7-43b8-9c45-8ffaf4f490df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 24,
    "yorig": 48
}