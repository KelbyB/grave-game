{
    "id": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a76fd7a-23aa-49d7-a593-cbf8654caed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "c3ffb859-5493-4cbf-9360-549cb2586072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a76fd7a-23aa-49d7-a593-cbf8654caed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b727ed0c-fdcb-4f6d-8ce6-b5d6b042cd53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a76fd7a-23aa-49d7-a593-cbf8654caed6",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        },
        {
            "id": "c8a317ae-84b6-44cf-a090-836acf30ec38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "75e5c9f7-56fc-4909-97a2-c4482d4b17df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a317ae-84b6-44cf-a090-836acf30ec38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0c735f-798b-47e4-b26c-bcc7440f188b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a317ae-84b6-44cf-a090-836acf30ec38",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        },
        {
            "id": "1e581c79-e18f-4ee2-bc55-84c08ee57345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "36aaf097-98d7-40ec-9fef-046da2fdbe0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e581c79-e18f-4ee2-bc55-84c08ee57345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e3134f-1981-49db-aa79-4544de4b9462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e581c79-e18f-4ee2-bc55-84c08ee57345",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        },
        {
            "id": "b65faabd-c8ce-499c-8edf-342499efa2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "c0e827b3-2045-4a56-a7bb-7cce9ba80d72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65faabd-c8ce-499c-8edf-342499efa2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "294a99a8-8c0d-4348-be77-246d8deccc7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65faabd-c8ce-499c-8edf-342499efa2a5",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        },
        {
            "id": "3d91ebca-27a3-4197-bfa4-9d8e08669f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "bccdc3f8-0f5c-41ac-8353-5598ded7bcc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d91ebca-27a3-4197-bfa4-9d8e08669f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888237b4-133d-4147-abd5-e0cf3ee67a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d91ebca-27a3-4197-bfa4-9d8e08669f6a",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        },
        {
            "id": "22bf89c8-61b0-48d4-8068-502462c156cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "compositeImage": {
                "id": "2150bbeb-3724-4682-af36-f57f772ff481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22bf89c8-61b0-48d4-8068-502462c156cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c6e386-095c-4f29-bfe3-de5b8c7f113f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22bf89c8-61b0-48d4-8068-502462c156cc",
                    "LayerId": "ec33f4ab-7b90-4556-bcbe-d54699f443d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ec33f4ab-7b90-4556-bcbe-d54699f443d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24ed439b-1a4c-400f-bd07-6ba0352e522a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 24,
    "yorig": 48
}