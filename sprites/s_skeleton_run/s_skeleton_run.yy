{
    "id": "2817a9de-fb67-4676-ba45-c4551d4e3994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 5,
    "bbox_right": 32,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dff55af-c826-4591-af0b-73d92f024534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "13a53084-ac92-44a3-ac73-16204eae98e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dff55af-c826-4591-af0b-73d92f024534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe5eac3-d555-45e9-b441-d2c3cc477375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dff55af-c826-4591-af0b-73d92f024534",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        },
        {
            "id": "57e1d37b-b657-489e-8931-ea48768ba706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "8ba6c24e-8d2f-4917-8258-cc5455ce64d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e1d37b-b657-489e-8931-ea48768ba706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf05bf6-b2f0-4a38-beb1-302121c349ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e1d37b-b657-489e-8931-ea48768ba706",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        },
        {
            "id": "84798c73-7cbf-4d66-b4b9-7faa47d6853c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "bf8b53d0-8c05-4651-b4f9-4aaf938ad60b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84798c73-7cbf-4d66-b4b9-7faa47d6853c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7908a4aa-f5db-48eb-86f2-3eadbd51c0a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84798c73-7cbf-4d66-b4b9-7faa47d6853c",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        },
        {
            "id": "6938bc1a-265b-4010-b85a-b8f83b6a6fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "60e17359-cb01-4418-98dc-91be2501e3bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6938bc1a-265b-4010-b85a-b8f83b6a6fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9de5fffc-08db-4527-996e-eca9284e8b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6938bc1a-265b-4010-b85a-b8f83b6a6fef",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        },
        {
            "id": "26f69e0b-70b5-4344-af0d-0be9354564e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "75df3f50-b19d-4da9-921a-86459613a571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f69e0b-70b5-4344-af0d-0be9354564e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73d7b4e-aa56-4f8a-81d3-89bfb99e791e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f69e0b-70b5-4344-af0d-0be9354564e2",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        },
        {
            "id": "f9ef2b5b-6530-41d6-b398-d2c2271575f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "compositeImage": {
                "id": "545bc006-254b-4064-9c30-61ad673f0335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9ef2b5b-6530-41d6-b398-d2c2271575f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c821be6a-1260-4454-a847-f5c536a9bafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9ef2b5b-6530-41d6-b398-d2c2271575f8",
                    "LayerId": "e7d5475f-578b-4c7e-8fff-a4373160aedf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e7d5475f-578b-4c7e-8fff-a4373160aedf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2817a9de-fb67-4676-ba45-c4551d4e3994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 48
}