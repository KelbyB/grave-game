{
    "id": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 76,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0a27835-91d4-4000-bbaa-66265240bc19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "compositeImage": {
                "id": "532fbb87-4960-4863-8e8b-a0ad552d43c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0a27835-91d4-4000-bbaa-66265240bc19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b690e47-05d7-4b3c-a985-5994b0e2d790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0a27835-91d4-4000-bbaa-66265240bc19",
                    "LayerId": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7"
                }
            ]
        },
        {
            "id": "4ee020de-7ca4-4e36-83d0-6516e4e9dc23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "compositeImage": {
                "id": "3e46e524-17f9-4d43-871e-06ba00123722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee020de-7ca4-4e36-83d0-6516e4e9dc23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3124ee09-3a4f-469a-9276-d51696d13fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee020de-7ca4-4e36-83d0-6516e4e9dc23",
                    "LayerId": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7"
                }
            ]
        },
        {
            "id": "fbb953de-99b9-42a4-89cb-494d7f9d4600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "compositeImage": {
                "id": "872099a9-ac01-437d-98b0-f74cccf0698e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb953de-99b9-42a4-89cb-494d7f9d4600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0958eb38-f5d2-4866-96f1-5e3fe78ae723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb953de-99b9-42a4-89cb-494d7f9d4600",
                    "LayerId": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7"
                }
            ]
        },
        {
            "id": "a4a755ee-9655-465b-afe4-ef47d25e5e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "compositeImage": {
                "id": "7b596332-5de5-4d79-82c2-f507c20ba129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4a755ee-9655-465b-afe4-ef47d25e5e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99ded88-251e-4cdf-a61b-b15b011ea9b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4a755ee-9655-465b-afe4-ef47d25e5e92",
                    "LayerId": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7"
                }
            ]
        },
        {
            "id": "67638a9f-1db7-47c9-aca8-1a1597022ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "compositeImage": {
                "id": "a3417bf2-b8a3-47c9-b5be-bf7659270e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67638a9f-1db7-47c9-aca8-1a1597022ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21b04ea-97dd-4087-af75-e4a27fc62618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67638a9f-1db7-47c9-aca8-1a1597022ca5",
                    "LayerId": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "78fcd560-ed5f-4b01-8250-d8d20a73a6b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c2e4bf4-923a-4ed8-b3ae-0a61be313ed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 48
}