{
    "id": "a3a0ffdf-b483-439c-99c1-d2442bd40f64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_three_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 36,
    "bbox_right": 90,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48a48d80-b2f7-4f2a-9b24-7ae5dfa15d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a0ffdf-b483-439c-99c1-d2442bd40f64",
            "compositeImage": {
                "id": "dd5c83ec-4d52-4966-a6f1-7c7e5e01b36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a48d80-b2f7-4f2a-9b24-7ae5dfa15d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5db28503-f55f-46db-9e9a-c1fd2603273b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a48d80-b2f7-4f2a-9b24-7ae5dfa15d37",
                    "LayerId": "72569515-b7e4-4f67-a1f3-e13a274e899a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "72569515-b7e4-4f67-a1f3-e13a274e899a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3a0ffdf-b483-439c-99c1-d2442bd40f64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 48
}