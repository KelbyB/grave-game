{
    "id": "e198df27-0026-40a7-90c1-5ac12dd92e35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25b903cd-7e26-4b9d-ae55-c97f44fe3b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e198df27-0026-40a7-90c1-5ac12dd92e35",
            "compositeImage": {
                "id": "2f209b58-d289-4b6d-b13c-08b070f6867c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b903cd-7e26-4b9d-ae55-c97f44fe3b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22100c8d-95b5-4f13-ab99-919c8efe4243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b903cd-7e26-4b9d-ae55-c97f44fe3b92",
                    "LayerId": "8e56f493-6725-4ea0-88b8-3c11f24b419b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "8e56f493-6725-4ea0-88b8-3c11f24b419b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e198df27-0026-40a7-90c1-5ac12dd92e35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 40
}