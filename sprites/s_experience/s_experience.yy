{
    "id": "566340f8-78fb-4947-9fc3-aef919f45cb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_experience",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "599379fa-1293-498e-b352-e336c2843410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "566340f8-78fb-4947-9fc3-aef919f45cb8",
            "compositeImage": {
                "id": "70f86a30-418e-462e-a9a4-9a01ad18041f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599379fa-1293-498e-b352-e336c2843410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87ff7c3-4dbb-4630-80b4-8ea7f5ec2958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599379fa-1293-498e-b352-e336c2843410",
                    "LayerId": "a8fd0c23-5c0d-4bf4-996d-2d47fbee24bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "a8fd0c23-5c0d-4bf4-996d-2d47fbee24bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "566340f8-78fb-4947-9fc3-aef919f45cb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}