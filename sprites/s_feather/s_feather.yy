{
    "id": "038ec158-3949-4fe6-a52f-aae9d06ff787",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_feather",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "627eafc2-0f56-4fef-894f-b82380340fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "038ec158-3949-4fe6-a52f-aae9d06ff787",
            "compositeImage": {
                "id": "36eefcc8-19db-46f7-8a91-f460ce70cafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "627eafc2-0f56-4fef-894f-b82380340fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9347fad7-b9f1-493c-abf5-797157bc05fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "627eafc2-0f56-4fef-894f-b82380340fd3",
                    "LayerId": "ac4e8ddf-e959-4c82-9781-d031e2c3c547"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "ac4e8ddf-e959-4c82-9781-d031e2c3c547",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "038ec158-3949-4fe6-a52f-aae9d06ff787",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}