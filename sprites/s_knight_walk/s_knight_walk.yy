{
    "id": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 33,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0cc862a-765f-40bb-a8a1-22a1863b9aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
            "compositeImage": {
                "id": "c102d9d5-2a4a-42d1-b370-a152497457a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0cc862a-765f-40bb-a8a1-22a1863b9aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57084df1-2bc7-4954-ac8e-842252fe37c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0cc862a-765f-40bb-a8a1-22a1863b9aab",
                    "LayerId": "bc9b7c0a-9755-4e8d-a63f-75474c521cdb"
                }
            ]
        },
        {
            "id": "b0d5123c-8067-4282-9d57-757c405d5612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
            "compositeImage": {
                "id": "b7384398-0507-412b-9eae-3b6533ba449d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0d5123c-8067-4282-9d57-757c405d5612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c23bb03-476f-4282-a60a-abad5759d77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0d5123c-8067-4282-9d57-757c405d5612",
                    "LayerId": "bc9b7c0a-9755-4e8d-a63f-75474c521cdb"
                }
            ]
        },
        {
            "id": "c8caf3f8-d9a6-4ae3-932f-a0db69b65eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
            "compositeImage": {
                "id": "023a27b6-f04e-48d5-bb57-9c75e8afe918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8caf3f8-d9a6-4ae3-932f-a0db69b65eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be33cfa3-ec18-4cd8-b2f8-2f8d86786bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8caf3f8-d9a6-4ae3-932f-a0db69b65eff",
                    "LayerId": "bc9b7c0a-9755-4e8d-a63f-75474c521cdb"
                }
            ]
        },
        {
            "id": "f5d0dec8-7151-4203-badc-9b84fec5c00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
            "compositeImage": {
                "id": "9802d56e-2660-43f2-ab7b-411daf3f731c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d0dec8-7151-4203-badc-9b84fec5c00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a544dda-07c9-4cb8-b50a-08ea9b4df0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d0dec8-7151-4203-badc-9b84fec5c00c",
                    "LayerId": "bc9b7c0a-9755-4e8d-a63f-75474c521cdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "bc9b7c0a-9755-4e8d-a63f-75474c521cdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b80bdb1c-7e6f-438f-ab68-af0be75d56a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 48
}