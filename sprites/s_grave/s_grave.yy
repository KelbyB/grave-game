{
    "id": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ead29b44-ef7c-4f76-8254-876143d21359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
            "compositeImage": {
                "id": "f75113d3-84a7-432d-aa69-bd186c18d1fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ead29b44-ef7c-4f76-8254-876143d21359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0d6255-892f-478a-945d-6ee0d46181d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead29b44-ef7c-4f76-8254-876143d21359",
                    "LayerId": "05927fe8-0d3e-405a-b152-3375af225472"
                }
            ]
        },
        {
            "id": "e3496cd8-79d5-46ea-8266-8ba9ac531216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
            "compositeImage": {
                "id": "5c71dd9b-94ce-4bf6-8aaa-0d1c69a57ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3496cd8-79d5-46ea-8266-8ba9ac531216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d5bdb5a-d2d3-4cc9-9a87-e01987a15293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3496cd8-79d5-46ea-8266-8ba9ac531216",
                    "LayerId": "05927fe8-0d3e-405a-b152-3375af225472"
                }
            ]
        },
        {
            "id": "e97cd9b4-396e-487c-899c-6b9d57e9fa91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
            "compositeImage": {
                "id": "9b8828cd-f858-476e-8411-307ece056ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97cd9b4-396e-487c-899c-6b9d57e9fa91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b540f89-515e-4f8b-9eb3-13de1a35d984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97cd9b4-396e-487c-899c-6b9d57e9fa91",
                    "LayerId": "05927fe8-0d3e-405a-b152-3375af225472"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "05927fe8-0d3e-405a-b152-3375af225472",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}