{
    "id": "b73a7e96-2680-49fb-9216-df447989bd4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_bones",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2bf794d-3154-4963-8c28-31d6120972da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "4bea2eee-6c93-4fef-9ea7-eb9315d0e7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bf794d-3154-4963-8c28-31d6120972da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9df119a7-61f6-4f46-a177-e9b6bf71c55a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bf794d-3154-4963-8c28-31d6120972da",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "c5d0f2c7-e41a-4c7b-ae36-5c069634146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "fce74e06-7205-4e49-9140-dda01439b578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d0f2c7-e41a-4c7b-ae36-5c069634146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2f1761-3816-4b83-881c-38e00ada60a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d0f2c7-e41a-4c7b-ae36-5c069634146f",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "6e478e32-c45f-4634-8495-570ee4a7455e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "e77f9b30-17a7-4a4d-a6aa-7befc2cb64df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e478e32-c45f-4634-8495-570ee4a7455e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9801518e-85ad-4209-8ab4-c0d8c01535d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e478e32-c45f-4634-8495-570ee4a7455e",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "1102a1e8-a18b-49cc-b08a-f45b185baade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "38188dc8-856d-4d88-8dc9-157816fdb814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1102a1e8-a18b-49cc-b08a-f45b185baade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9dea6b7-72fe-4adc-98d4-a8d0fb72568e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1102a1e8-a18b-49cc-b08a-f45b185baade",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "ac6d5155-b941-4220-9315-8e7585224e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "af2a7f41-8385-49cc-a04c-0cba42155d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6d5155-b941-4220-9315-8e7585224e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6d3373-15f7-49b7-814f-b630b5f35638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6d5155-b941-4220-9315-8e7585224e65",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "1abae6d3-190c-4613-b1ec-e672d3723566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "f5b59aa1-bc36-46c4-a4f5-7da304c1bb6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1abae6d3-190c-4613-b1ec-e672d3723566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb4fd090-4e25-4e73-a554-0476dfdf51c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1abae6d3-190c-4613-b1ec-e672d3723566",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "1d2c5d5c-7353-40be-99e7-b6a0208cd85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "53fdd621-7813-47a9-bff3-62ba9f2ea80a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2c5d5c-7353-40be-99e7-b6a0208cd85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa3537f-520b-4c92-988a-ba72891f3b2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2c5d5c-7353-40be-99e7-b6a0208cd85c",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "f748f1a3-8034-4624-ad97-3c61c367a739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "bb7b88c5-3668-4bbc-9765-4f1514f3d9df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f748f1a3-8034-4624-ad97-3c61c367a739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6964c37-5a5f-441e-b35e-854bb2770fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f748f1a3-8034-4624-ad97-3c61c367a739",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "d91a0896-8c98-4edf-893b-f499f50630b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "86f29fc2-7ef3-43e9-9cc2-e8d4310d64fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91a0896-8c98-4edf-893b-f499f50630b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c3e9c1-d533-4f6d-a071-10f50c901758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91a0896-8c98-4edf-893b-f499f50630b4",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        },
        {
            "id": "d350097a-25ec-4fb0-893b-666c3fe5a2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "compositeImage": {
                "id": "cfbf6132-bdf8-4a1e-8dc2-10713395f1f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d350097a-25ec-4fb0-893b-666c3fe5a2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b47495f-8319-41db-adbd-5a98d0096b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d350097a-25ec-4fb0-893b-666c3fe5a2f4",
                    "LayerId": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f0e1835f-a0ff-4fa8-8863-f6a3969c6f9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 10
}