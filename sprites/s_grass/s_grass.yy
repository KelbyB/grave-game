{
    "id": "7ac84bdb-997c-4194-bd13-86dadf2cde30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90b3381a-c893-4bb2-a671-560eb178fe84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ac84bdb-997c-4194-bd13-86dadf2cde30",
            "compositeImage": {
                "id": "927975b9-574d-428e-a96d-d3ef31807448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b3381a-c893-4bb2-a671-560eb178fe84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03902010-3762-40d7-92b2-fcccde2cffdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b3381a-c893-4bb2-a671-560eb178fe84",
                    "LayerId": "83753edf-68ff-4826-bf46-915264b95880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "83753edf-68ff-4826-bf46-915264b95880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ac84bdb-997c-4194-bd13-86dadf2cde30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}