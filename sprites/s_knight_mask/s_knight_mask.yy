{
    "id": "1906d7a0-56b9-4fad-8385-e8db9e94cc7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dcc53ab-3843-4459-bc1c-95f93b3b2608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1906d7a0-56b9-4fad-8385-e8db9e94cc7b",
            "compositeImage": {
                "id": "9974607f-9aaf-464b-8729-10321dd67175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dcc53ab-3843-4459-bc1c-95f93b3b2608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa4e1123-8c6d-47b4-971b-4ab8a6c76236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dcc53ab-3843-4459-bc1c-95f93b3b2608",
                    "LayerId": "9ec49f1f-c5c0-491b-bd30-0ee66403b373"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "9ec49f1f-c5c0-491b-bd30-0ee66403b373",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1906d7a0-56b9-4fad-8385-e8db9e94cc7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 40
}