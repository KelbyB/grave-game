{
    "id": "eca1fc66-21c8-4069-8201-a7652878729f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2e5d238-81fd-405e-9271-e4d3b632142b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca1fc66-21c8-4069-8201-a7652878729f",
            "compositeImage": {
                "id": "d271d08a-9b60-45ee-9d02-f3a0ac6d95ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e5d238-81fd-405e-9271-e4d3b632142b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ee7600-38d9-480b-8033-701b2b303684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e5d238-81fd-405e-9271-e4d3b632142b",
                    "LayerId": "82ff5bf2-41fe-46be-9cd3-2461e0f135d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "82ff5bf2-41fe-46be-9cd3-2461e0f135d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eca1fc66-21c8-4069-8201-a7652878729f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}