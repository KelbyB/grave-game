{
    "id": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 21,
    "bbox_right": 90,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "480d05d9-c89e-4b93-8f6a-55a830b80457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "192e0af0-c12b-4e86-b33e-b3120523c4e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480d05d9-c89e-4b93-8f6a-55a830b80457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b786aa6-8385-4c5a-8ae8-cb839e9e48f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480d05d9-c89e-4b93-8f6a-55a830b80457",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        },
        {
            "id": "5f208afb-e40f-4250-95be-264a0eb12328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "8ec99718-e39f-42ab-afc7-f8c87b5e3ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f208afb-e40f-4250-95be-264a0eb12328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126f016d-9822-4c33-8386-fdced1dbf26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f208afb-e40f-4250-95be-264a0eb12328",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        },
        {
            "id": "be1c7566-c9ca-4bcb-b208-1fa8bb312193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "46330a45-56d8-46f7-b505-fb57ebfbb212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1c7566-c9ca-4bcb-b208-1fa8bb312193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb4d72ec-a5de-4a51-8850-c6089a525382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1c7566-c9ca-4bcb-b208-1fa8bb312193",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        },
        {
            "id": "2ebe2b55-8219-478a-976e-9c2d53deb318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "546ede25-dac0-4b31-9d4e-de0481346175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebe2b55-8219-478a-976e-9c2d53deb318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9240fd4b-7c2f-48ae-ad92-0b6f736a5ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebe2b55-8219-478a-976e-9c2d53deb318",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        },
        {
            "id": "d0087132-682b-40ed-8936-1a0992c97b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "cb989c63-1580-48d3-a5bb-7a4cb8c0b6be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0087132-682b-40ed-8936-1a0992c97b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830398a1-bc0a-479f-84aa-51e84206b6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0087132-682b-40ed-8936-1a0992c97b22",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        },
        {
            "id": "cd3772b6-63ee-4416-8b31-78a77ea6d0d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "compositeImage": {
                "id": "e99853fc-f009-48d0-9323-198b4ed76258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3772b6-63ee-4416-8b31-78a77ea6d0d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ae9b70-335a-4fb1-afb9-4d54a04300d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3772b6-63ee-4416-8b31-78a77ea6d0d7",
                    "LayerId": "01b75cc9-a297-45e4-a4e4-95c5f835ca42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "01b75cc9-a297-45e4-a4e4-95c5f835ca42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fa84671-c8c8-4d3c-b2f8-1b7896298b1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 48
}