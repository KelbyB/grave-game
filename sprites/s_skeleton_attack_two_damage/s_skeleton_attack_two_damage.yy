{
    "id": "a3ae6610-2739-4b57-8ca6-658feadcb385",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_two_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 16,
    "bbox_right": 74,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6a7dc67-a931-4add-b503-a03ea86ac43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3ae6610-2739-4b57-8ca6-658feadcb385",
            "compositeImage": {
                "id": "04c7276b-4599-4594-bf32-560313388e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a7dc67-a931-4add-b503-a03ea86ac43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df6c5ca0-a801-4f1a-8110-61cf0698f02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a7dc67-a931-4add-b503-a03ea86ac43c",
                    "LayerId": "5b781e1a-97ec-4461-bae9-af496c80c43f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5b781e1a-97ec-4461-bae9-af496c80c43f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3ae6610-2739-4b57-8ca6-658feadcb385",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 48
}