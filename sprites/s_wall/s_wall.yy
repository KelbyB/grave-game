{
    "id": "a0c63281-ee7f-43dc-ab9f-a430ab1d7ff5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "413ed365-346b-4aee-8a1a-87a422fcbd85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c63281-ee7f-43dc-ab9f-a430ab1d7ff5",
            "compositeImage": {
                "id": "a2404f79-0304-4acd-8a25-742fab04ac2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413ed365-346b-4aee-8a1a-87a422fcbd85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d66694-00a0-4b4d-8b40-86485948b030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413ed365-346b-4aee-8a1a-87a422fcbd85",
                    "LayerId": "0c535d63-057a-479c-a26c-2ae461dab2f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c535d63-057a-479c-a26c-2ae461dab2f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0c63281-ee7f-43dc-ab9f-a430ab1d7ff5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}