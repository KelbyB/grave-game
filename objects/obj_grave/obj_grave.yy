{
    "id": "21777edb-00b2-4572-9bc6-9925e4df43a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grave",
    "eventList": [
        {
            "id": "da8bc468-0243-4c1c-8437-dfdbe63037a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21777edb-00b2-4572-9bc6-9925e4df43a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b122d4e8-fcec-475f-98a7-905f7dcfc452",
    "visible": true
}