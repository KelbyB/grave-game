{
    "id": "95fcc287-c24e-4fde-a1ff-e9c4203ce6be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crow",
    "eventList": [
        {
            "id": "343c5585-313c-48f3-8ab6-fa0cb0f5d093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95fcc287-c24e-4fde-a1ff-e9c4203ce6be"
        },
        {
            "id": "60b96c37-24a7-4f22-b5bd-08b7556e2915",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95fcc287-c24e-4fde-a1ff-e9c4203ce6be"
        },
        {
            "id": "58f6e040-dcef-4b03-af9a-52d46b6e538d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "95fcc287-c24e-4fde-a1ff-e9c4203ce6be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "15920d48-23b2-453f-b896-0598ff4bc066",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1be2b772-21b7-4364-af0c-8402a1495376",
    "visible": true
}