switch (state) {
	case "Chase":
		#region Chase
		set_state_sprite(s_knight_walk, 0.4, 0);
		if not instance_exists(obj_skeleton) break;
		
		image_xscale = sign(obj_skeleton.x - x);
		if image_xscale == 0 {
			image_xscale = 1;
		}
		
		var direction_facing = image_xscale
		var distance_to_player = point_distance(x, y, obj_skeleton.x, obj_skeleton.y)
		
		if distance_to_player <= attack_range {
			state = "Attack";
		}
		else {
			move_and_collide(direction_facing * chase_speed, 0);
		}
		#endregion
		break;
	case "Attack":
		#region Attack
		set_state_sprite(s_knight_attack, 0.6, 0);
		
		if animation_hit_frame(4) {
			create_hitbox(x, y, self, s_skeleton_attack_one_damage, 4, 4, 10, image_xscale);
		}
		
		if animation_end() {
			state = "Chase";
		}
		#endregion
		break;
	case "Knockback":
		#region Knockback
		knockback_state(s_knight_hitstun, "Chase");
		#endregion
		break;
	case "Death":
		#region Death
			death_state(s_knight_die);
		#endregion
		break;
	default: 
		show_debug_message("State " +state+ " does not exist");
		state = "Chase";
		break;
}