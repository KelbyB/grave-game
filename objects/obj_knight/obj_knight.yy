{
    "id": "08f20afa-d7f0-4c25-931c-60af91ae9359",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_knight",
    "eventList": [
        {
            "id": "2a2faa41-a439-4cd0-825e-5afc3b7ec425",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08f20afa-d7f0-4c25-931c-60af91ae9359"
        },
        {
            "id": "3e23f533-d76f-45a9-96f3-36ab856687f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08f20afa-d7f0-4c25-931c-60af91ae9359"
        }
    ],
    "maskSpriteId": "1906d7a0-56b9-4fad-8385-e8db9e94cc7b",
    "overriddenProperties": null,
    "parentObjectId": "15920d48-23b2-453f-b896-0598ff4bc066",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a5c8229-488b-46cb-9774-272a5cd043d1",
    "visible": true
}