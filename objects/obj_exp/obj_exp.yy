{
    "id": "33b4d488-b0c1-4776-94a7-97fd9c93b757",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exp",
    "eventList": [
        {
            "id": "9249fc32-3aa6-482c-b894-9972655609de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33b4d488-b0c1-4776-94a7-97fd9c93b757"
        },
        {
            "id": "bf349610-8a10-460c-9743-86aefd3de297",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33b4d488-b0c1-4776-94a7-97fd9c93b757"
        },
        {
            "id": "06bfcd46-fcf7-40b7-9315-e36dd394effd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "33b4d488-b0c1-4776-94a7-97fd9c93b757",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "33b4d488-b0c1-4776-94a7-97fd9c93b757"
        },
        {
            "id": "084c2a69-3be9-4567-9ff1-f99120139fb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3323319f-af44-4c78-a308-9cd8bde2d5e6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "33b4d488-b0c1-4776-94a7-97fd9c93b757"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "bf0c6439-b646-4cc1-91e6-25465fd46f4e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4c1c7eda-077c-4b53-8a20-c0a090394102",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 0
        },
        {
            "id": "726b716a-11d4-4136-b93a-e2388fba95e4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "ac2a2068-797a-4195-b8dc-d163dddcd789",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "566340f8-78fb-4947-9fc3-aef919f45cb8",
    "visible": true
}