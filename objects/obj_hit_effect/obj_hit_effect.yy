{
    "id": "5663a126-97c5-43e0-b69f-179a893f90e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hit_effect",
    "eventList": [
        {
            "id": "358fcbd8-7a2d-48e4-b838-6ccdb71de54f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5663a126-97c5-43e0-b69f-179a893f90e7"
        },
        {
            "id": "a8f03600-3d2f-4080-9527-c35a32ccfa12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5663a126-97c5-43e0-b69f-179a893f90e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91d92920-8b8a-4b8b-89a1-12b710091774",
    "visible": true
}