{
    "id": "87bfc53b-4bf7-4abb-959e-4d3b48c9f509",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_feather",
    "eventList": [
        {
            "id": "1a2c5e11-6b6d-45e9-876a-9e073e4cdd92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87bfc53b-4bf7-4abb-959e-4d3b48c9f509"
        },
        {
            "id": "60fc9a48-b1fa-463e-9b68-47b53b4fcc33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87bfc53b-4bf7-4abb-959e-4d3b48c9f509"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "038ec158-3949-4fe6-a52f-aae9d06ff787",
    "visible": true
}