var enemy_count = instance_number(obj_enemy_parent);

if instance_exists(obj_skeleton) and enemy_count < obj_skeleton.kills/4 {
	if enemy_count > 5 {
		exit;
	}
	
	var enemy = choose(obj_knight, obj_crow, obj_crow);
	
	var new_x = random_range(220, room_width - 220);
	while point_distance(new_x, 0, obj_skeleton.x, 0) < 200 {
		new_x = random_range(220, room_width - 220);
	}
	
	instance_create_layer(new_x, obj_skeleton.y, "Instances", enemy);

}