{
    "id": "ed68ca59-a62f-4174-9220-1ebbadb9ed9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_parallax",
    "eventList": [
        {
            "id": "1617ed08-9a95-45c7-873b-3193e4834b1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ed68ca59-a62f-4174-9220-1ebbadb9ed9b"
        },
        {
            "id": "d31cdd36-9b67-43a7-a1fb-92f0d2af236a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ed68ca59-a62f-4174-9220-1ebbadb9ed9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6fbf1d9a-3be8-42a2-b91f-e281a5d2745d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FF666666",
            "varName": "close_gray",
            "varType": 7
        },
        {
            "id": "6e0b5189-5156-44c8-9778-3606d3fdadf5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FF7F7F7F",
            "varName": "far_gray",
            "varType": 7
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}