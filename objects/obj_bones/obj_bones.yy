{
    "id": "f6e5b9b7-5998-4bb5-a503-367642a1d9d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bones",
    "eventList": [
        {
            "id": "208f0e78-8446-423a-9532-47ed06747745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6e5b9b7-5998-4bb5-a503-367642a1d9d5"
        },
        {
            "id": "3dfb6bb9-0ff7-4c35-a477-5bb7781f8feb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "05ce8970-fd88-4128-952b-d667f1f03725",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f6e5b9b7-5998-4bb5-a503-367642a1d9d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b73a7e96-2680-49fb-9216-df447989bd4e",
    "visible": true
}