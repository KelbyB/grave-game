{
    "id": "15920d48-23b2-453f-b896-0598ff4bc066",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_parent",
    "eventList": [
        {
            "id": "43af8e5b-92b6-45ce-8932-d0f841d86092",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "15920d48-23b2-453f-b896-0598ff4bc066"
        },
        {
            "id": "5f72c707-c5d5-4d4f-8945-b692f983c0c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "15920d48-23b2-453f-b896-0598ff4bc066"
        },
        {
            "id": "02fb4249-dd99-4e47-9f57-4a2c6ef6f0e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "15920d48-23b2-453f-b896-0598ff4bc066"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f2e12abb-21f8-455c-a6e5-8f54cdeb1c3c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}