{
    "id": "6451b235-28ae-4269-8b41-40bd2d4d170f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitbox",
    "eventList": [
        {
            "id": "eba00ccf-9227-4d7d-bfe1-56b98420f83d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f2e12abb-21f8-455c-a6e5-8f54cdeb1c3c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6451b235-28ae-4269-8b41-40bd2d4d170f"
        },
        {
            "id": "4a722776-9a1e-4d3d-85e5-e42fc6a20072",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6451b235-28ae-4269-8b41-40bd2d4d170f"
        },
        {
            "id": "851f4c0e-768c-447c-8cc8-31592f22517f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6451b235-28ae-4269-8b41-40bd2d4d170f"
        },
        {
            "id": "6fc73005-93a6-407e-ac72-9e51f54d8bc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6451b235-28ae-4269-8b41-40bd2d4d170f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}