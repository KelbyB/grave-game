{
    "id": "3323319f-af44-4c78-a308-9cd8bde2d5e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_skeleton",
    "eventList": [
        {
            "id": "6f35ca64-c2ce-4f15-8416-d08db0455937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3323319f-af44-4c78-a308-9cd8bde2d5e6"
        },
        {
            "id": "80019eb0-15de-4b81-bd22-9714120899e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3323319f-af44-4c78-a308-9cd8bde2d5e6"
        }
    ],
    "maskSpriteId": "e198df27-0026-40a7-90c1-5ac12dd92e35",
    "overriddenProperties": null,
    "parentObjectId": "f2e12abb-21f8-455c-a6e5-8f54cdeb1c3c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1aede0d7-f6c3-4eef-a3af-a4e062263170",
    "visible": true
}