
switch (state) {
	case "Move":
		#region Move State
		if input.right {
			move_and_collide(run_speed, 0);
			image_xscale = 1;
			sprite_index = s_skeleton_run;
			image_speed = 0.6;
		}
		if input.left {
			move_and_collide(-run_speed, 0);
			image_xscale = -1;
			sprite_index = s_skeleton_run;
			image_speed = 0.6;
		}
		if not input.right and not input.left {
			sprite_index = s_skeleton_idle;
			image_speed = 0.4;
		}
			if input.roll {
			state = "Roll";
		}
			if input.attack {
			state = "AttackOne";
		}
		#endregion
		break;
	case "Roll":
		#region Roll State
		set_state_sprite(s_skeleton_roll, 0.7, 0)
	
		if image_xscale == 1 {
			move_and_collide(roll_speed, 0)
		}
	
		if image_xscale == -1 {
			move_and_collide(-roll_speed, 0)
		}
		
		if animation_end() {
			state = "Move"
		}
		#endregion
		break;
	case "AttackOne":
		#region AttackOne State
		set_state_sprite(s_skeleton_attack_one, 0.7, 0);
		
		if animation_hit_frame(0) {
			create_hitbox(x, y, self, s_skeleton_attack_one_damage, 3, 4, 5, image_xscale);
		}
		
		if input.attack and animation_hit_frame_range(2, 4) {
			state = "AttackTwo";
		}
		
		if animation_end() {
			state = "Move"
		}
		#endregion
		break;
	case "AttackTwo":
		#region AttackTwo State
		set_state_sprite(s_skeleton_attack_two, 0.7, 0);
		
		if animation_hit_frame(1) {
			create_hitbox(x, y, self, s_skeleton_attack_two_damage, 3, 4, 5, image_xscale);
		}
		
		if input.attack and animation_hit_frame_range(2, 4) {
			state = "AttackThree";
		}
		
		if animation_end() {
			state = "Move";
		}
		#endregion
		break;
	case "AttackThree":
		#region AttackThree State
		set_state_sprite(s_skeleton_attack_three, 0.7, 0);
		
		if animation_hit_frame(2) {
			create_hitbox(x, y, self, s_skeleton_attack_three_damage, 4, 4, 5, image_xscale);
		}
		
		if animation_end() {
			state = "Move";
		}
		#endregion
		break;
	case "Knockback":
		#region Knockback
		knockback_state(s_skeleton_hitstun, "Move");
		#endregion
		break;
	default: 
		show_debug_message("State " +state+ " does not exist");
		state = "Move";
		break;
}