event_inherited();
image_speed = 0.4;
state = "Move";
run_speed = 3.5;
roll_speed = 4;
kills = 0;
level = 1;
experience = 0;
max_experience = 10;
strength = 25;

// Dependencies
input = instance_create_layer(0, 0, "Instances", obj_input);