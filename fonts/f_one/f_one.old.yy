{
    "id": "e53337c1-c39a-460b-b4ef-291d0984c8df",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_one",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f5b73b38-68e4-452a-ac6b-2a865b3525e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "15e8102f-4c54-4d2d-b16d-43998da4b838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7b8cb7da-1bcf-4f5a-98c2-fc759ee31101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9431260d-ed6c-4bce-860a-7841c9399870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "23701310-cb84-4191-9896-5d926935fea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7f193e92-c952-440d-a4dd-f987291cc197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 44,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3639eb61-2338-4537-af33-1d9682cd15f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e8b676ea-dc79-4309-9da4-dbc07870cf50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "58a99042-bcc7-4374-9303-004b2e1a095a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 25,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3f05d747-6204-4914-a453-f772cfe66afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 20,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c054678b-94dc-4720-868a-9f6bb1d9b3dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b328d545-6969-4ca8-aab7-1ae6cecb4548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "74b01bb1-40f2-4c73-a029-660fe3dfa608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 21
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bdf1ae29-b551-4c15-a9da-4721720b3f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 21
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3c4dcc07-040b-46b5-9977-49d4a823b126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 234,
                "y": 21
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6ba81ed0-e2de-4526-b63f-2a41d1c5c5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 228,
                "y": 21
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "27d943c8-286d-4ea5-941e-d343c7c7d504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 219,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fea7eccc-53d8-412c-a19b-de3a1d7bef46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 213,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7d298fcd-8bb3-497f-a08e-2cab7e154b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 203,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "85366871-c182-4c61-abc5-e18d6d2aef85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0d25ec1f-6afe-4562-aed0-dcf829e778b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 184,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5bd6131c-82ca-4fc0-a04a-ca2110c5b166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c10215c7-3383-47d5-aec2-00ccb5a3a941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 93,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ae0db81a-628c-4a4d-82f7-e0b1165ebc90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 102,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c7f9ab4e-b5d9-433c-8634-42f16dc2c7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0a693240-225a-4d75-ba09-8a6b4ee7265b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 62,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "74036430-1382-4daf-a758-49040ca088b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 58,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ebefd969-7198-44d9-9249-fba483fd2305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 54,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d7853185-7efd-48fb-b8ae-80307503f90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 45,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "850aeb3c-010e-4e6b-b37e-571f24681420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 36,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ea73a5a9-fc9f-4f84-b716-50369ba2e606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8e775fe1-1734-47b6-9efa-2e6e4e1feb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9fc540a3-2cb1-4f48-b6a3-5daec441333c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ed493d3e-127a-4cb9-8b74-4e62d1c661c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 238,
                "y": 40
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a36683d4-6a0f-464b-ba55-0e819fd34871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 228,
                "y": 40
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e522a9a1-01bb-4087-a3e3-3c8cfab7c336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e35f9123-ec7a-430f-b7d3-20f7ed7651e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 206,
                "y": 40
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c7447b27-e879-4c03-ab7c-e6920b545afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 196,
                "y": 40
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bab0659c-426e-403b-b673-1ac89ba811fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 187,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "04f4b03c-873e-4ab9-ba6f-75756fdbab6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 175,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "99b12188-a43f-4e72-a8f0-25787557c896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 164,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "02b98bd2-14da-462d-8179-9e9ca553a22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 160,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e48109c5-5326-4b8f-b61a-a2d29848c49a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 152,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "370f0e39-527f-4e02-83c2-6448c2e56e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 141,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4c248a62-cc2c-4953-9bc4-36a0570e50d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 132,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9c82f26c-8f56-4050-82fd-a5d80adbea79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 120,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a660364e-99f7-4044-a502-a5874bf40379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 173,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "501d4622-5f37-41d1-a9c7-6f109d790590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 161,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "30f2e35f-c08c-4a6e-aaa3-f6dbb68f9c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 151,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "13938fcb-42d2-4256-b420-3a7c2f544461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "49b478bf-8698-40b6-b736-2e8a83efda66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2e85e4ed-3201-4a18-b6c1-42790b2fb217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fbab0b29-6758-4d70-a803-c411df1817f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1140ccae-bc6f-4b10-a4f2-582027bdbf8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "169eb438-eb31-4ed6-aedc-2b8115c5f423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8d7bdb3f-ebe0-4a59-9716-e0bdc1f75b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c42a252c-43ac-4afd-9f84-9414712bda9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "768db92a-0477-40fd-a3ad-6270f93db5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f7e3be5d-7690-48e2-a39f-a6522dd7e6d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c9214efe-44e1-44da-8628-7fd9d6d44875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "035d8afe-dfec-4c3f-b3d6-9238a2222134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "535cd89a-ee81-4909-a5ff-a1677aa5361f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2a223644-d0cc-486f-93ff-48266f5ccb29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "221a7580-c53e-4d43-b7a5-93153bc784fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "10b447ec-13d7-4027-a25e-ed165cc6deee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6cd527d2-b80b-4f9f-a6d2-bf8e42070566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9520d950-9aed-4b85-8910-d58caf88d956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "30d057d7-31ed-4bd8-bf03-03fb522b3762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c6a8d9e9-5ffb-493b-96d7-b8d58e58c8a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bbd73471-6de8-4751-afa9-3d6ff1dfc812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "707c21b9-860f-45e0-9dee-e9fccf2ee78f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bc659a86-b42f-4e5b-9b3a-283a20d3aa7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4f370c82-cbd6-427d-ab75-f1dd271084d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 59,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5fd7bab5-785e-4d41-887c-5b82918977a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "baa1eb96-8e8c-4ea6-baa1-7825993d31c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 137,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2d8c2f66-f85d-41aa-9a83-069ffd4d8503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 129,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "243d31e7-825b-456e-b7c6-718b794d175e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 126,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6d4dd9e9-b5f4-4245-aace-7c8a6d9b08fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 113,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f2254cc6-6d5d-4079-939e-1cd7e22ef0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 105,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2379936a-cb95-4f5c-8092-a4774df3a4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 96,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8a92d2c6-4158-4818-9ed8-45d4f377449e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 87,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7f4b6b89-1f61-497b-b0b9-4aa03c9ca2ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "074f3c53-5081-4c0b-bc95-84408ec079be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 73,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dccc3fdf-69c9-48df-811f-642a304c5f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 142,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a3d7fb00-5fd3-4385-8a37-0e7aed200f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 67,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b1c52d4a-706f-40f8-8908-877d313ee6e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 51,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7e12edf4-ebab-4821-9891-6f5442bb78b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 42,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c4a22db6-fee1-4c7b-9e2b-3b411ec50a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 29,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3117350a-f529-4e5f-8503-6d64b0503569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "551d8b45-3fed-45bc-a37f-dac29a021e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9c66e250-0ee8-45ce-9f37-65a3f6144e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "50e7d7a1-2543-40a7-9945-85bdfae08554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "79294264-b850-4153-b46a-3caab382175e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a99071c3-6b8f-4cb8-b62f-55c94fd10b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "16b8dfba-2c72-4397-8ff4-668c507840e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3ddb0367-01db-4ed2-84f2-f7604c3c0af6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 80,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f1b063a2-aba4-4700-9f9e-e5d106243e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "3fb19690-c0ec-4776-a85c-46b23ce72690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "36324918-9470-48dc-ac55-1133cde9b825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "5fe25f57-ec69-4014-a79d-9c219d22e99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "025eb762-3ec1-48fa-bc98-0899b572da95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "89b00f24-a08a-4ce5-9fd1-c53537dbea8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "b6e6a88b-2f20-4e91-aa34-5940cdb21c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "9791bee2-60a3-442e-ba92-a44d87091617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "2e880089-697c-4e5a-a533-bf50c7930ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "42579645-5320-4c24-9470-21b90b98e54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "5f1cb1a1-7b52-44dd-a93e-4b2f6fe8d297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "82ff5878-3f80-452b-af19-4c72d87c2632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "ef06e019-e775-479a-8ae5-62d96b288657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "ef6b3148-2cec-43f1-88a7-ffd1730015db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "3f1b6b35-0adb-43eb-b93a-518e6950cfc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "bf0a8446-fbea-4931-97bb-a10d3e23dec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "ccea668b-f935-4d1e-93ab-77bb73f82061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "60c8b3e3-87d7-458a-854d-5d9f76b64528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c74faf4c-4d63-4249-a9b8-35b77756d99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "5b930798-0f6f-4426-8f66-dac8430ff494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "b28adb1c-3724-40ec-b59b-9fd2cac7720c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "c54f5e10-7168-4964-8ef6-6d89ffe4a28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ec957761-b588-425d-a684-5435b607a0a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "1f5c7d04-2797-4d40-ad25-4bde26fb0c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "15b2e7ae-25fe-46ac-9b91-e019e505c08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "fddea1d8-8afa-49c6-8650-c552d8c27894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "76342c93-6dfb-4505-af67-6e55e994545a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "adde6c1b-a5a3-4aa9-b875-50d6456b240d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "49ec4f0e-8595-47f9-b235-391fe9337f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a629c27c-9d94-4aa4-9585-22859bb8a3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "ac412497-a7ea-427a-8929-2288b10c3f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "c076845c-bf2d-4fa8-86f5-76957320787d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "cd5da119-fdd9-4e87-baaf-5b0450cae3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "810deb07-8b0b-41ff-a2db-1d92950eb980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "974af981-08be-4139-a8b8-9f326148ca5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "05c873a0-5df0-497f-80aa-f7c75719b303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b8166a7a-29cb-4fb0-9871-34108b967be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "53660334-133e-4607-88a5-2207cc5e5346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "54469494-50f1-452e-af49-0edba938ec0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "6caa4657-e984-48ed-bf93-a4fe9deebd67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "aa0444d1-2e78-4514-aeb9-0ca4b0404e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "82a1aaeb-22c6-44e1-91dd-8287983d0708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "a9b23f3c-7f56-4752-92ee-6d9e6993de14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "84aed118-6ec2-4114-bcc0-685e3eef9e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "cc02fbb9-df8f-41ef-9103-ab43963a3afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 11,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}