{
    "id": "e53337c1-c39a-460b-b4ef-291d0984c8df",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_one",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "54761d67-458d-43f6-aa38-55ca57eabd26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "96e53020-a1c7-4ecd-87f8-723232bc6394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e46d8091-5944-4583-8539-52a89f4084c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 118,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fbd7d97d-5b8d-4c8b-8236-0e6cd0015e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0fd77b7a-594a-46b1-91e9-cd6200e06801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "19a428fd-01b2-472d-beb8-e18e3595e895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e7f31787-0b11-4241-ab22-dcdc37ec1aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4d34b1f3-596d-4c1e-912f-9181c100fb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 1,
                "shift": 2,
                "w": 1,
                "x": 78,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "df09c111-1ca5-464f-93c4-9c4d1c84d383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 73,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9b3cd71d-dc1c-4650-980a-792ddd1b1805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "14d431b8-3a2c-4069-a196-f8485c1e32de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f8e1dbbc-0675-4794-91a1-8cb21b66cd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 61,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "eeafb882-640d-4fce-a9e0-4925ab5880f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 51,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8e74578f-75c5-45b5-a157-cb0aba160158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5a270d26-5850-429a-8c6b-7532a7bb480a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 42,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5f1a20e2-3f58-4106-8d51-168d25477f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9aa4ab87-9975-4336-ad42-7785f2db836f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ee5c823f-533a-47f6-b98d-5a95dbbf524a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a6c2265d-a2ae-4de0-bba8-cfc7421268ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "53e74a1e-5571-41d1-836c-18d6d3e67f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 10,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5fdd835d-600e-4506-9409-a3b4cb626b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "abdc6f26-51b5-4ceb-9f1f-997f2afe243b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "51754322-f741-4dba-8a5b-bc7f191ef0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 8,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8d719b32-49d9-4f6e-8e85-2524ac603e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ba974dd5-f682-412d-a499-a9bffa1dfb13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 23,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e92ba1c3-7875-4838-9aed-24d78a61c728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 67,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4811a0f7-fb5f-488c-bf53-51bba340e136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 64,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eced2039-79a1-4e30-8ed4-e65c390764e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 61,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "880c900e-5e5d-4602-8b29-49bf903df64d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9ac22154-b568-4faa-88c5-e31cfd2d98d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 47,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4b1df98e-b987-46fd-9c8d-cf0906bfc7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "02c634d5-09b0-4d9b-8c3b-493a6e620957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 33,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3d867459-b777-4c64-b5b9-c8a74fd3725a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 20,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2c93e7ff-fa0d-4f76-a68b-bbe78bce355d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 10,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c2051ac0-e235-4b57-91e4-dd820e6ad13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "90157918-a27e-447c-9fb4-d129adb2f21f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 111,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9641d76b-a568-4b2e-9655-72c4c4a96e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 66
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4f58afa5-be66-4cd7-a785-1e4c19498619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 94,
                "y": 66
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9044426a-7696-4ac0-ba39-76eb3639cb29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 66
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6b328036-c708-48ae-8989-8ffb77de9f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 76,
                "y": 66
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7a1b109a-a8f0-46f5-b677-320a0118376d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 67,
                "y": 66
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fa5f8028-4ca1-4f7f-bd1d-be35042ea39a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 64,
                "y": 66
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f6d556ad-8964-48a0-937e-b439c7b650df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 57,
                "y": 66
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "77e7b92f-d611-485e-a01b-a72df5167ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 66
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "509691dd-e9c3-42ee-a927-056be518754e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 41,
                "y": 66
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "67a32511-623d-473e-90bb-13412d60e822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 31,
                "y": 66
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "90f71d79-5ce8-4d89-a7af-ca9f4249c210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 115,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0c747d15-289f-4898-9193-e09720c1b058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 105,
                "y": 34
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b3279973-bc7b-4d54-90bf-ca158ce56504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "443e4fa0-5d77-4d42-858c-368f150f5694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 18
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "84f34aa5-8341-4aac-a1ea-ba5f52159325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1e65c27d-3715-4fce-af51-dc13bf304a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 30,
                "y": 18
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "000538f8-a932-4706-8c35-e1ab569d7dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 18
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8f2f766b-eea1-43f3-b975-3776a0d394e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 12,
                "y": 18
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2c6dcc42-93c3-43d2-89d8-bf4536bc33f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5b396dc1-30d6-49ec-8508-17f3438811bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ca5b2c0e-280f-4248-bd80-588c1028a6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9f9f8223-8e94-415d-a986-fa15f03acd50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "da7992a2-9930-483e-aad4-c2f6dad3588d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "19ef3789-0dd8-4546-8845-edf0789c8733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 48,
                "y": 18
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5b066d91-9fe5-4b41-bd04-194670bc0edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "aaa7c64d-898e-45e3-9f2d-ad3eba8a1aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e04c5d17-e870-4649-a3a8-245bbf74a4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "875aaef6-d853-49a7-bf6d-01f3197158ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "aaede36a-612b-4a58-a1a2-5326333717b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5f5cb384-53f7-4375-a118-97a77a9e755d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e4cf80bd-4270-4ae1-a28e-636761d5e133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "209190b7-7b4c-4312-87d8-47356d0b8a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6b7f1047-5521-452c-958c-ffc70189dc94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a8315ed6-f9fe-4e49-b0df-45b633aa3d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "223491e6-3e7d-49bf-9eb1-0178b5939a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e53dfc9a-ae91-496d-85b9-c7f4a01f3f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "64113022-d2db-42b5-9a6f-01d050308a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 34
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "aeb9f2a0-13cb-4a39-a436-573b83d73025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 70,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "38476c88-8501-488d-9553-702c73ccea2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 84,
                "y": 34
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "388fdefd-b18c-4622-b766-a1c023b8f51d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 34
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a7dcc2c1-6d9a-4fcf-83be-71f8071472b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 74,
                "y": 34
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c2864056-3b2b-4a2b-9e82-4ebb4dcfa832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 64,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c1ccb5e6-b881-4266-8c8b-437a6e785aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 57,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "212cecf4-2c96-4c61-9554-d0f9aeb0027f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 34
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "edad0f7b-3c3b-4e2a-8e4c-3021b93377cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 42,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f6879f45-84c4-4c40-9b33-4b604b73a0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 34
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1a71116b-6403-4241-9e96-164abcb691cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 29,
                "y": 34
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9c87a087-aee9-4640-8f3c-c1dfd2a138f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 34
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3fd892f8-5cfb-401b-9324-48a1907c76a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 24,
                "y": 34
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f662fbcb-398b-4039-8e4f-e165156782db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 10,
                "y": 34
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "14711bb9-07c8-4af8-912b-0505b8e82780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "abd227a8-8d82-41ad-b128-1f7d9bdb0517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 113,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f19c0409-cebb-4686-9707-97715afefbe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 105,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "df906a42-2385-4cbb-ab1f-b0c5de715b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "eb45a170-3b03-45de-a92e-355f1118cbb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "af88615c-8d07-4885-8e5a-8a5496918543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 83,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "baeb1362-d243-486d-bce0-738de1e65ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 79,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "107de3cf-7dc4-4de9-ba90-3a089d92f1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 73,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b7afe1d1-e52a-4b35-a809-d650d8aaf29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b0a38cf3-dd1d-423c-8787-edf21743ecce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 82,
                "y": 82
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "bb055bfc-0aca-4193-aadf-bb04d028f516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "cb329e3e-5ccd-4f05-9e2f-d2c8edf9e996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "5e1de222-0c98-46de-b2ee-3d7fb6f3993f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d2ee46ea-ac39-4d0b-8861-3be37ae1bbb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "857c1a6e-bd0f-4295-b169-4efbb8b66a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "5c26c7d7-5ab5-4bff-a4e9-f9082e480d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "b77e06c2-9604-4a32-823c-b700073b9872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "a815f179-85cf-4207-820e-14fcf06095f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "5b719ec1-984e-4f37-a2d1-d7d3b7839be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e63b0865-8f06-4ac8-ab1a-bfbb82f5fdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "e583d1dd-affe-4889-b16e-eef661608289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ba5f7f5d-0f48-4c83-9f7a-b86f5efd9fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "babcec85-e971-4288-9946-e87eb73cfdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "aec23a50-a0fb-4d28-8d99-33be408f6a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "37e4624a-1415-4a2d-ae61-46dc8f6ae411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "1c29a0b8-327a-4fbb-9b25-188e782cebe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "ca3188db-f1d7-45f8-a128-e61b6e8f292c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "b97ec0d9-e690-46db-9773-e245dc3a7a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "f459b8fc-5ffb-4085-aa8b-986f2fd8fbff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "088ed198-db5f-4e76-aa73-7fdd17758583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "3c4c26e4-4337-4b11-9d50-db7d1d4a9ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "3a2ec395-4f39-4467-9ad8-06a26fef7d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "fbe57918-00c4-450a-904b-90f60654de74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}